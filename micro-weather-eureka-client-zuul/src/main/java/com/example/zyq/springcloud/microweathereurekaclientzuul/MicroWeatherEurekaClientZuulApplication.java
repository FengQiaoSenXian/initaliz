package com.example.zyq.springcloud.microweathereurekaclientzuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableZuulProxy
public class MicroWeatherEurekaClientZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroWeatherEurekaClientZuulApplication.class, args);
	}

}
