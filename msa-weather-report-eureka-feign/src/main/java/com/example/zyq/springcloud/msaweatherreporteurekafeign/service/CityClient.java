package com.example.zyq.springcloud.msaweatherreporteurekafeign.service;

import com.example.zyq.springcloud.msaweatherreporteurekafeign.vo.City;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("msa-weather-city-eureka")
public interface CityClient {
    @GetMapping("/cities")
    List<City> listCity() throws Exception;
}
