package com.example.zyq.springcloud.msaweatherreporteurekafeign.service;

import com.example.zyq.springcloud.msaweatherreporteurekafeign.vo.Weather;

public interface WeatherReportService {
    /**
     * 根据城市ID查询天气信息
     * @param cityId
     * @return
     */
    Weather getDataByCityId(String cityId);
}
