package com.example.zyq.springcloud.msaweatherreporteurekafeign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsaWeatherReportEurekaFeignApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaWeatherReportEurekaFeignApplication.class, args);
	}

}
