package com.example.zyq.springcloud.msaweatherreporteurekafeigngateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsaWeatherReportEurekaFeignGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaWeatherReportEurekaFeignGatewayApplication.class, args);
	}

}
