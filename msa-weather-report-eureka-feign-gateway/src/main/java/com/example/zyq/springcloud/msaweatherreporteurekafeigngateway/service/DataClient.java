package com.example.zyq.springcloud.msaweatherreporteurekafeigngateway.service;

import com.example.zyq.springcloud.msaweatherreporteurekafeigngateway.vo.City;
import org.springframework.cloud.openfeign.FeignClient;

import java.util.List;
@FeignClient("msa-weather-eureka-client-zuul")
public interface DataClient {
    List<City> listCity();
}
