package com.example.zyq.springcloud.msaweatherreporteurekafeigngateway.service;

public interface WeatherReportService {
    Object getDataByCityId(String cityId);
}
