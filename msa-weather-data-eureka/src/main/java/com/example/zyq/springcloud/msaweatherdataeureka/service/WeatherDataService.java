package com.example.zyq.springcloud.msaweatherdataeureka.service;

import com.example.zyq.springcloud.msaweatherdataeureka.vo.WeatherResponse;

public interface WeatherDataService {
    WeatherResponse getDataByCityId(String cityId);

    WeatherResponse getDataByCityName(String cityName);
}
