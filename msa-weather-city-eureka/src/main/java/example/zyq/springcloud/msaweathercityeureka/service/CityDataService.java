package example.zyq.springcloud.msaweathercityeureka.service;

import example.zyq.springcloud.msaweathercityeureka.vo.City;

import java.util.List;

public interface CityDataService {
    List<City> listCity()throws Exception ;
}
