package com.example.zyq.springcloud.microweathereurekaclient.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HiController {

    //向配置中心读取配置的端口消息
    //访问：http://localhost:8762/hi?name=forezp
    //页面展示结果：hi forezp,i am from port:我zyq8762
    @Value("${server.port}")
    String port;

    @GetMapping("/hi")
    public String home(@RequestParam String name) {
        return "hi " + name + ",i am from port:我zyq" + port;

    }
}
