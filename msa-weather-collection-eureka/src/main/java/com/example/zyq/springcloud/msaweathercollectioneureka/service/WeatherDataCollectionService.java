package com.example.zyq.springcloud.msaweathercollectioneureka.service;

public interface WeatherDataCollectionService  {
    void syncDateByCityId(String cityId);
}
